package com.example.coroutinesroom.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.coroutinesroom.model.LoginState
import com.example.coroutinesroom.model.UserDB
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class LoginViewModel(application: Application) : AndroidViewModel(application) {

    private val coroutineScope = CoroutineScope(Dispatchers.IO)
    private val db by lazy { UserDB(getApplication()).userDao() }

    val loginComplete = MutableLiveData<Boolean>()
    val error = MutableLiveData<String>()

    fun login(username: String, password: String) {
        coroutineScope.launch {
            val user = db.getUser(username)

            when {
                user == null -> {
                    withContext(Dispatchers.Main) { error.value = "User not found" }
                }
                user.passwordHash == password.hashCode() -> {
                    LoginState.login(user)
                    withContext(Dispatchers.Main) { loginComplete.value = true }
                }
                else -> {
                    withContext(Dispatchers.Main) { error.value = "Passwors is incorrect" }
                }
            }
        }
    }
}