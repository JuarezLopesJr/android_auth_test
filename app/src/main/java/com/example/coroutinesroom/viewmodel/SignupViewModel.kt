package com.example.coroutinesroom.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.coroutinesroom.model.LoginState
import com.example.coroutinesroom.model.User
import com.example.coroutinesroom.model.UserDB
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class SignupViewModel(application: Application) : AndroidViewModel(application) {

    private val coroutineScope = CoroutineScope(Dispatchers.IO)
    private val db by lazy { UserDB(getApplication()).userDao() }

    val signupComplete = MutableLiveData<Boolean>()
    val error = MutableLiveData<String>()

    fun signup(userName: String, password: String, info: String) {
        coroutineScope.launch {
            val user = db.getUser(userName)
            if (user != null) {
                withContext(Dispatchers.Main) {
                    error.value = "User already exist"
                }
            } else {
                val user = User(userName, password.hashCode(), info)
                val userId = db.insertUser(user)
                user.id = userId
                LoginState.login(user)

                withContext(Dispatchers.Main) {
                    signupComplete.value = true
                }
            }
        }
    }

}